import 'dart:io';

class Menu {
  String? menuSelect;
  String? sweetLV;
  int total = 0;
  start() {
    print("Please select number of your drink category");
    print("\n1.Coffee");
    print("2.Tea");
    print("3.Milk Cocoa Caramel");
    print("4.Protein shake");
    print("5.Soda and more");
    print("6.Recommended");
    int category = int.parse(stdin.readLineSync()!);
    switch (category) {
      case 1:
        coffee();
        sweetLv();
        break;
      case 2:
        tea();
        sweetLv();
        break;
      case 3:
        milk();
        sweetLv();
        break;
      case 4:
        protein();
        sweetLv();
        break;
      case 5:
        soda();
        sweetLv();
        break;
      case 6:
        recommended();
        break;
    }
  }

  coffee() {
    print("\nPlease select type of your drink ");
    print("Ice press 1");
    print("Hot press 2");
    int HC = int.parse(stdin.readLineSync()!);
    if (HC == 1) {
      hotCoffee();
    } else {
      iceCoffee();
    }
  }

  tea() {
    print("\nPlease select type of your drink ");
    print("Ice press 1");
    print("Hot press 2");
    int HC = int.parse(stdin.readLineSync()!);
    if (HC == 1) {
      hotTea();
    } else {
      icedTea();
    }
  }

  milk() {
    print("\nPlease select type of your drink ");
    print("Ice press 1");
    print("Hot press 2");
    int HC = int.parse(stdin.readLineSync()!);
    if (HC == 1) {
      hotMilk();
    } else {
      icedTea();
    }
  }

  hotCoffee() {
    print("\n1.Esspresso");
    print("2.Double Esspresso");
    print("3.Hot Americano");
    print("4.Hot Latte");
    print("5.Hot Capuccino");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 45;
        return menuSelect = "Esspresso";
      case 2:
        total = 45;
        return menuSelect = "Double Esspresso";
      case 3:
        total = 45;
        return menuSelect = "Hot Americano";
      case 4:
        total = 45;
        return menuSelect = "Hot Latte";
      case 5:
        total = 45;
        return menuSelect = "Hot Capuccino";
    }
  }

  iceCoffee() {
    print("\n1.Esspresso");
    print("2.Double Esspresso");
    print("3.Iced Americano");
    print("4.Iced Latte");
    print("5.Iced Capuccino");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 50;
        return menuSelect = "Esspresso";
      case 2:
        total = 50;
        return menuSelect = "Double Esspresso";
      case 3:
        total = 50;
        return menuSelect = "Iced Americano";
      case 4:
        total = 50;
        return menuSelect = "Iced Latte";
      case 5:
        total = 50;
        return menuSelect = "Iced Capuccino";
    }
  }

  hotTea() {
    print("\n1.Hot Chrysanthemum Tea");
    print("2.Hot Thai Milk Tea");
    print("3.Hot Taiwan Tea");
    print("4.Hot Matchalatte Milk Tea");
    print("5.Hot Kokuto Tea");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 45;
        return menuSelect = "Hot Chrysanthemum Tea";
      case 2:
        total = 45;
        return menuSelect = "Hot Thai Milk Tea";
      case 3:
        total = 45;
        return menuSelect = "Hot Taiwan Tea";
      case 4:
        total = 45;
        return menuSelect = "Hot Matchalatte Milk Tea";
      case 5:
        total = 45;
        return menuSelect = "Hot Kokuto Tea";
    }
  }

  icedTea() {
    print("\n1.Iced Chrysanthemum Tea");
    print("2.Iced Thai Milk Tea");
    print("3.Iced Taiwan Tea");
    print("4.Iced Matchalatte Milk Tea");
    print("5.Iced Kokuto Tea");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 50;
        return menuSelect = "Iced Chrysanthemum Tea";
      case 2:
        total = 50;
        return menuSelect = "Iced Thai Milk Tea";
      case 3:
        total = 50;
        return menuSelect = "Iced Taiwan Tea";
      case 4:
        total = 50;
        return menuSelect = "Iced Matchalatte Milk Tea";
      case 5:
        total = 50;
        return menuSelect = "Iced Kokuto Tea";
    }
  }

  hotMilk() {
    print("\n1.Hot Caramel Milk");
    print("2.Hot Kokuta Milk");
    print("3.Hot Cocoa");
    print("4.Hot Caramel Cocoa");
    print("5.Hot milk");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 45;
        return menuSelect = "Hot Caramel Milk";
      case 2:
        total = 45;
        return menuSelect = "Hot Kokuta Milk";
      case 3:
        total = 45;
        return menuSelect = "Hot Caramel Cocoa";
      case 4:
        total = 45;
        return menuSelect = "Hot Caramel Cocoa";
      case 5:
        total = 45;
        return menuSelect = "Hot milk";
    }
  }

  icedMilk() {
    print("\n1.Iced Caramel Milk");
    print("2.Iced Kokuta Milk");
    print("3.Iced Cocoa");
    print("4.Iced Caramel Cocoa");
    print("5.Iced milk");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 50;
        return menuSelect = "Iced Caramel Milk";
      case 2:
        total = 50;
        return menuSelect = "Iced Kokuta Milk";
      case 3:
        total = 50;
        return menuSelect = "Iced Caramel Cocoa";
      case 4:
        total = 50;
        return menuSelect = "Iced Caramel Cocoa";
      case 5:
        total = 50;
        return menuSelect = "Iced milk";
    }
  }

  protein() {
    print("\nPlease select menu of your drink ");
    print("1.Plain Protein Shake");
    print("2.Matcha Protein Shake");
    print("3.Cocoa Protein Shake");
    print("4.Taiwanese Tea Protein Shake");
    print("5.Milk Protein Shake");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 55;
        return menuSelect = "Plain Protein Shake";
      case 2:
        total = 55;
        return menuSelect = "Matcha Protein Shake";
      case 3:
        total = 55;
        return menuSelect = "Cocoa Protein Shake";
      case 4:
        total = 55;
        return menuSelect = "Taiwanese Tea Protein Shake";
      case 5:
        total = 55;
        return menuSelect = "Milk Protein Shake";
    }
  }

  soda() {
    print("\nPlease select menu of your drink ");
    print("1.Pepsi");
    print("2.Iced Limenade Soda");
    print("3.Iced Lychee Soda");
    print("4.Iced Strawberry Soda");
    print("5.Iced Sala Soda");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 30;
        return menuSelect = "Pepsi";
      case 2:
        total = 30;
        return menuSelect = "Iced Limenade Soda";
      case 3:
        total = 30;
        return menuSelect = "Iced Lychee Soda";
      case 4:
        total = 30;
        return menuSelect = "Iced Strawberry Soda";
      case 5:
        total = 30;
        return menuSelect = "Iced Sala Soda";
    }
  }

  recommended() {
    print("\nPlease select menu of your drink ");
    print("1.Mozart Almost Dirty");
    print("2.Iced Caramel Cafe Latte");
    print("3.Iced Kokuto Milk");
    print("4.Iced Blueberry Tea");
    print("5.Iced Lychee");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        total = 45;
        return menuSelect = "Mozart Almost Dirty";
      case 2:
        total = 50;
        return menuSelect = "Iced Caramel Cafe Latte";
      case 3:
        total = 50;
        return menuSelect = "Iced Kokuto Milk";
      case 4:
        total = 50;
        return menuSelect = "Iced Blueberry Tea";
      case 5:
        total = 50;
        return menuSelect = "Iced Lychee";
    }
  }

  sweetLv() {
    print("\nPlease enter number your sweet level");
    print("1.No sugar");
    print("2.Sugar less");
    print("3.Normal");
    print("4.High Sugar");
    print("5.Damn High Sugar!!");
    int select = int.parse(stdin.readLineSync()!);
    switch (select) {
      case 1:
        sweetLV = "No sugar";
        return total += 0;
      case 2:
        sweetLV = "Sugar less";
        return total += 5;
      case 3:
        sweetLV = "Normal";
        return total += 5;
      case 4:
        sweetLV = "High Sugar";
        return total += 5;
      case 5:
        sweetLV = "Damn High Sugar!!";
        return total += 10;
    }
  }

  printString() {
    print("\nYour Order : " + menuSelect.toString() + " with " + sweetLV.toString());
    print("Total price of your order : " + total.toString());
  }
}
